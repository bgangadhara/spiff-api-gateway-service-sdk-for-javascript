## Description
Precor Connect spiff api gateway service SDK for javascript.

## Features

##### Post spiff entitlements with partnerrep info
* [documentation](features/ClaimSpiffEntitlements.feature)

##### Get partner Reps Information
* [documentation](features/ListPartnerRepsInfoWithAccountId.feature)

##### Get spiff entitlements
* [documentation](features/ListSpiffEntitlementsAccountId.feature)

##### Post upload partnersale invoice
* [documentation](features/UploadInvoiceForPartnerSaleRegistration.feature)


## Setup

**install via jspm**  
```shell
jspm install spiff-api-gateway-service-sdk=bitbucket:precorconnect/spiff-api-gateway-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import SpiffApiGatewayServiceSdk,{SpiffApiGatewayServiceSdkConfig} from 'spiff-api-gateway-service-sdk'

const spiffApiGatewayServiceSdkConfig =
    new SpiffApiGatewayServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const spiffApiGatewayServiceSdk =
    new SpiffApiGatewayServiceSdk(
        spiffApiGatewayServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```