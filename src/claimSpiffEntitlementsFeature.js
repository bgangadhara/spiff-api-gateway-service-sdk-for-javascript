import {inject} from 'aurelia-dependency-injection';
import SpiffApiGatewayServiceSdkConfig from './spiffApiGatewayServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import ClaimSpiffWebView from './claimSpiffWebView';
import SpiffEntitlementWithPartnerRepInfoWebDto from './spiffEntitlementWithPartnerRepInfoWebDto';

@inject(SpiffApiGatewayServiceSdkConfig, HttpClient)
class ClaimSpiffEntitlementsFeature {

    _config:SpiffApiGatewayServiceSdkConfig;

    _httpClient:HttpClient;

    _cachedClaimSpiffs:Array<ClaimSpiffWebView>;

    constructor(config:SpiffApiGatewayServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Post the spiff entitlements with partnerrep info
     * @param {string} accountId
     * @param {SpiffEntitlementWithPartnerRepInfoWebDto[]} request
     * @param {string} accessToken
     * @returns {Promise.<ClaimSpiffWebView[]>} claimSpiffWebView
     */
    execute(accountId:string,
            request:SpiffEntitlementWithPartnerRepInfoWebDto[],
            accessToken:string):Promise<ClaimSpiffWebView[]> {

        return this._httpClient
            .createRequest(`spiff-api-gateway/claimspiffentitlements/${accountId}`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withHeader('Content-Type', `application/json`)
            .withContent(request)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => {
                // cache
                this._cachedClaimSpiffs =
                    Array.from(
                        response.content,
                        contentItem =>
                            new ClaimSpiffWebView(
                                contentItem.claimSpiffId,
                                contentItem.partnerSaleRegistrationId,
                                contentItem.accountId,
                                contentItem.userId,
                                contentItem.firstName,
                                contentItem.lastName,
                                contentItem.installDate,
                                contentItem.spiffAmount,
                                contentItem.spiffClaimedDate,
                                contentItem.facilityName,
                                contentItem.invoiceNumber
                            )
                    );

                return this._cachedClaimSpiffs;

            });
    }
}

export default ClaimSpiffEntitlementsFeature;