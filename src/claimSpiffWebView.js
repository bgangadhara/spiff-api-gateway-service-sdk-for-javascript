/**
 * @class {ClaimSpiffWebView}
 */
export default class ClaimSpiffWebView {

     _claimSpiffId:number;

     _partnerSaleRegistrationId:number;

     _accountId:string;

     _userId:string;

     _firstName:string;

     _lastName:string;

     _installDate:string;

     _spiffAmount:number;

     _spiffClaimedDate:string;

     _facilityName:string;

     _invoiceNumber:string;


    /**
     * @param {number} claimSpiffId
     * @param {number} partnerSaleRegistrationId
     * @param {string} accountId
     * @param {string} userId
     * @param {string} firstName
     * @param {string} lastName
     * @param {string} installDate
     * @param {number} spiffAmount
     * @param {string} spiffClaimedDate
     * @param {string} facilityName
     * @param {string} invoiceNumber
     */
    constructor(claimSpiffId:number,
                partnerSaleRegistrationId:number,
                accountId:string,
                userId:string,
                firstName:string,
                lastName:string,
                installDate:string,
                spiffAmount:number,
                spiffClaimedDate:string,
                facilityName:string,
                invoiceNumber:string
    ) {

        if (!claimSpiffId) {
            throw new TypeError('claimSpiffId required');
        }
        this._claimSpiffId = claimSpiffId;

        if (!partnerSaleRegistrationId) {
            throw new TypeError('partnerSaleRegistrationId required');
        }
        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

        if (!accountId) {
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

        if (!userId) {
            throw new TypeError('userId required');
        }
        this._userId = userId;

        this._installDate = installDate;

        if (!spiffAmount) {
            throw new TypeError('spiffAmount required');
        }
        this._spiffAmount = spiffAmount;

        if (!spiffClaimedDate) {
            throw new TypeError('spiffClaimedDate required');
        }
        this._spiffClaimedDate = spiffClaimedDate;

        if (!facilityName) {
            throw new TypeError('facilityName required');
        }
        this._facilityName = facilityName;

        if (!invoiceNumber) {
            throw new TypeError('invoiceNumber required');
        }
        this._invoiceNumber = invoiceNumber;

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

    }

    /**
     * @returns {number}
     */
     get claimSpiffId():number {
        return this._claimSpiffId;
    }

    /**
     * @returns {number}
     */
     get partnerSaleRegistrationId():number {
        return this._partnerSaleRegistrationId;
    }

    /**
     * @returns {string}
     */
     get accountId():string {
        return this._accountId;
    }

    /**
     * @returns {string}
     */
     get userId():string {
        return this._userId;
    }

    /**
     * @returns {string}
     */
    get firstName():string {
        return this._firstName;
    }

    /**
     * @returns {string}
     */
    get lastName():string {
        return this._lastName;
    }

    /**
     * @returns {string}
     */
     get installDate():string {
        return this._installDate;
     }

    /**
     * @returns {number}
     */
     get spiffAmount():number {
        return this._spiffAmount;
     }

    /**
     * @returns {string}
     */
     get spiffClaimedDate():string {
        return this._spiffClaimedDate;
     }

    /**
     * @returns {string}
     */
     get facilityName():string {
        return this._facilityName;
     }

    /**
     * @returns {string}
     */
     get invoiceNumber():string {
        return this._invoiceNumber;
     }

    toJSON() {
        return {
            claimSpiffId:this._claimSpiffId,
            partnerSaleRegistrationId: this._partnerSaleRegistrationId,
            accountId: this._accountId,
            userId: this._userId,
            firstName:this._firstName,
            lastName:this._lastName,
            installDate: this._installDate,
            spiffAmount: this._spiffAmount,
            spiffClaimedDate: this._spiffClaimedDate,
            facilityName: this._facilityName,
            invoiceNumber: this._invoiceNumber
        };
    }
}