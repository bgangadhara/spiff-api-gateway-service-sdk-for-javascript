import {inject} from 'aurelia-dependency-injection';
import SpiffApiGatewayServiceSdkConfig from './spiffApiGatewayServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import SpiffEntitlementWithPartnerRepInfoWebView from './spiffEntitlementWithPartnerRepInfoWebView';

@inject(SpiffApiGatewayServiceSdkConfig, HttpClient)
class ListSpiffEntitlementsAccountIdFeature {

    _config:SpiffApiGatewayServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:SpiffApiGatewayServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Get the spiff entitlements
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {Promise.<SpiffEntitlementWithPartnerRepInfoWebView[]>} spiffEntitlementWithPartnerRepInfoWebView
     */
    execute(accountId:string,
            accessToken:string):Promise<SpiffEntitlementWithPartnerRepInfoWebView[]> {

        return this._httpClient
            .createRequest(`spiff-api-gateway/spiffentitlements/${accountId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => {
                // cache
                return Array.from(
                        response.content,
                        contentItem =>
                            new SpiffEntitlementWithPartnerRepInfoWebView(
                                contentItem.spiffEntitlementId,
                                contentItem.partnerSaleRegistrationId,
                                contentItem.installDate,
                                contentItem.spiffAmount,
                                contentItem.partnerRepUserId,
                                contentItem.invoiceUrl,
                                contentItem.invoiceNumber,
                                contentItem.facilityName,
                                contentItem.firstName,
                                contentItem.lastName,
                                contentItem.bankInfoExists,
                                contentItem.w9InfoExists,
                                contentItem.contactInfoExists,
                                contentItem.sellDate
                            )
                    );


            });
    }
}

export default ListSpiffEntitlementsAccountIdFeature;