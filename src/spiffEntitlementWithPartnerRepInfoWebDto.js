/**
 * @class {SpiffEntitlementWithPartnerRepInfoWebDto}
 */
export default class SpiffEntitlementWithPartnerRepInfoWebDto {

    _spiffEntitlementId:number;

    _partnerSaleRegistrationId:number;

    _installDate:string;

    _spiffAmount:number;

    _partnerRepUserId:string;

    _invoiceUrl:string;

    _invoiceNumber:string;

    _facilityName:string;

    _sellDate:string


    /**
     * @param {number} spiffEntitlementId
     * @param {number} partnerSaleRegistrationId
     * @param {string} installDate
     * @param {number} spiffAmount
     * @param {string} partnerRepUserId
     * @param {string} invoiceUrl
     * @param {string} invoiceNumber
     * @param {string} facilityName
     * @param {string} sellDate
     */
    constructor(spiffEntitlementId:number,
                partnerSaleRegistrationId:number,
                installDate:string,
                spiffAmount:number,
                partnerRepUserId:string,
                invoiceUrl:string,
                invoiceNumber:string,
                facilityName:string,
                sellDate:string
    ) {

        if (!spiffEntitlementId) {
            throw new TypeError('spiffEntitlementId required');
        }
        this._spiffEntitlementId = spiffEntitlementId;

        if (!partnerSaleRegistrationId) {
            throw new TypeError('partnerSaleRegistrationId required');
        }
        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

        this._installDate = installDate;

        if (!spiffAmount) {
            throw new TypeError('spiffAmount required');
        }
        this._spiffAmount = spiffAmount;

        this._partnerRepUserId = partnerRepUserId;

        this._invoiceUrl = invoiceUrl;

        if (!invoiceNumber) {
            throw new TypeError('invoiceNumber required');
        }
        this._invoiceNumber = invoiceNumber;

        if (!facilityName) {
            throw new TypeError('facilityName required');
        }
        this._facilityName = facilityName;

        if (!sellDate) {
            throw new TypeError('sellDate required');
        }
        this._sellDate = sellDate;

    }

    /**
     * @returns {number}
     */
    get spiffEntitlementId():number {
        return this._spiffEntitlementId;
    }

    /**
     * @returns {number}
     */
    get partnerSaleRegistrationId():number {
        return this._partnerSaleRegistrationId;
    }

    /**
     * @returns {string}
     */
    get installDate():string {
        return this._installDate;
    }

    /**
     * @returns {number}
     */
    get spiffAmount():number {
        return this._spiffAmount;
    }

    /**
     * @returns {string}
     */
    get partnerRepUserId():string {
        return this._partnerRepUserId;
    }

    /**
     * @returns {string}
     */
    get invoiceUrl():string {
        return this._invoiceUrl;
    }

    /**
     * @returns {string}
     */
    get invoiceNumber():string {
        return this._invoiceNumber;
    }

    /**
     * @returns {string}
     */
    get facilityName():string {
        return this._facilityName;
    }

    /**
     * @returns {string}
     */
    get sellDate():string{
        return this._sellDate;
    }

    toJSON() {
        return {
            spiffEntitlementId:this._spiffEntitlementId,
            partnerSaleRegistrationId: this._partnerSaleRegistrationId,
            installDate: this._installDate,
            spiffAmount: this._spiffAmount,
            partnerRepUserId: this._partnerRepUserId,
            invoiceUrl: this._invoiceUrl,
            invoiceNumber: this._invoiceNumber,
            facilityName: this._facilityName,
            sellDate : this._sellDate
        };
    }
}