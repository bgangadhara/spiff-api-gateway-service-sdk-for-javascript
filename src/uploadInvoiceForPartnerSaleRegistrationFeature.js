import {inject} from 'aurelia-dependency-injection';
import SpiffApiGatewayServiceSdkConfig from './spiffApiGatewayServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import UploadPartnerSaleInvoiceReqWebDto from './uploadPartnerSaleInvoiceReqWebDto';
import PartnerSaleInvoiceWebView from './partnerSaleInvoiceWebView';

@inject(SpiffApiGatewayServiceSdkConfig, HttpClient)
class UploadInvoiceForPartnerSaleRegistrationFeature {

    _config:SpiffApiGatewayServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:SpiffApiGatewayServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Post the upload partnersale invoice
     * @param {UploadPartnerSaleInvoiceReqWebDto} request
     * @param {string} accessToken
     * @returns {Promise.<PartnerSaleInvoiceWebView>} partnerSaleInvoiceWebView
     */
    execute(request:UploadPartnerSaleInvoiceReqWebDto,
            accessToken:string):Promise<PartnerSaleInvoiceWebView> {

        return this._httpClient
            .createRequest(`spiff-api-gateway/uploadinvoice`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({"partnerSaleInvoiceNumber":request.partnerSaleInvoiceNumber, "partnerSaleRegId":request.partnerSaleRegId})
            .withContent(request.file)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => {
                  return new PartnerSaleInvoiceWebView(
                                response.partnerSaleInvoiceId,
                                response.partnerSaleInvoiceNumber,
                                response.fileUrl,
                                response.partnerSaleRegId
                            );

            });
    }
}

export default UploadInvoiceForPartnerSaleRegistrationFeature;