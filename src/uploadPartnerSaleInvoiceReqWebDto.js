/**
 * @class {UploadPartnerSaleInvoiceReqWebDto}
 */
export default class UploadPartnerSaleInvoiceReqWebDto {

     _partnerSaleRegId:number;

     _partnerSaleInvoiceNumber:string;

     _file:FormData;


    /**
     * @param {number} partnerSaleRegId
     * @param {string} partnerSaleInvoiceNumber
     * @param {string} file
     */
    constructor(partnerSaleRegId:number,
                partnerSaleInvoiceNumber:string,
                file:FormData
    ) {

        if (!partnerSaleRegId) {
            throw new TypeError('partnerSaleRegId required');
        }
        this._partnerSaleRegId = partnerSaleRegId;

        if (!partnerSaleInvoiceNumber) {
            throw new TypeError('partnerSaleInvoiceNumber required');
        }
        this._partnerSaleInvoiceNumber = partnerSaleInvoiceNumber;

        if (!file) {
            throw new TypeError('file required');
        }
        this._file = file;

    }

    /**
     * @returns {number}
     */
     get partnerSaleRegId():number {
        return this._partnerSaleRegId;
    }

    /**
     * @returns {string}
     */
     get partnerSaleInvoiceNumber():string {
        return this._partnerSaleInvoiceNumber;
    }

    /**
     * @returns {string}
     */
     get file():FormData {
        return this._file;
    }

    toJSON() {
        return {
            partnerSaleRegId:this._partnerSaleRegId,
            partnerSaleInvoiceNumber: this._partnerSaleInvoiceNumber,
            file: this._file
        };
    }
}